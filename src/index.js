const { Client, Attachment } = require('discord.js');
var events = require('events');
var https = require('https');
var fs = require('fs');
var url = require('url');
var http = require('http');
var exec = require('child_process').exec;
var spawn = require('child_process').spawn;



const client = new Client();
var commands = new events.EventEmitter();
var audioChannel = {};


client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});


//Triggers every time a message is sent in a chat
client.on('message', msg => {
  //get the first word the message
  var command = msg.content.split(' ')[0];
  //if the first letter of the word is !, trigger an event with the event
  if(command[0] === '!'){
    commands.emit(command.substr(1), msg);
  }
});
commands.on('helpfate', msg=>{
  msg.reply("Here is a quick list of commads:\r\n -!dice: Will roll the dices for you. \r\n -!voice: Will make the bot join your voice channel \r\n -!save {filename}: Will save the file you sent with the specified filename \r\n -!playfile {file}: Will paly the audio file over the voice channel")
});
commands.on('voice', msg=>{
  if (msg.member.voiceChannel) {
      msg.member.voiceChannel.join()
        .then(connection => { // Connection is an instance of VoiceConnection
          msg.reply(`I have successfully connected to the channel ${msg.member.voiceChannel}!`);
          audioChannel[msg.channel.parentID] = connection;
        })
        .catch(console.log);
    } else {
      msg.reply('You need to join a voice channel first!');
    }
});
commands.on('playfile', msg=>{
  var audioFile = msg.content.split(' ')[1];
  const dispatcher = audioChannel[msg.channel.parentID].playFile('assets/audio/user_audio/'+msg.channel.parentID+'/'+audioFile);
});

commands.on('save', msg=>{
  msg.reply("received files!");
  file_name=msg.content.split(' ')[1];
  if(file_name===undefined){
      file_name = url.parse(msg.attachments.array()[0].url).pathname.split('/').pop();
  }
  var file = 'assets/audio/user_audio/'+msg.channel.parentID+'/'+file_name;
  var mkdir = 'mkdir -p ' + 'assets/audio/user_audio/'+msg.channel.parentID;
  var child = exec(mkdir, function(err, stdout, stderr) {
    if (err) throw err;
  });
  download_file_curl(msg.attachments.array()[0].url, file);
  msg.reply("Download finnished!")
  //console.log(typeof msg.attachments);

});

//Trigger every time a !dice command is sent in a chat
commands.on('dice', msg=>{
  let dice1 = rollDice(3);
  let dice2 = rollDice(3);
  let dice3 = rollDice(3);
  let dice4 = rollDice(3);
  let total = diceToStats(dice1)+diceToStats(dice2)+diceToStats(dice3)+diceToStats(dice4);
  //create a string to be sent in the message
  let reply = `rolls the dice, obtainig ${signPrint(diceToStats(dice1))}  ${signPrint(diceToStats(dice2))}  ${signPrint(diceToStats(dice3))}  ${signPrint(diceToStats(dice4))}, gaining a total of ${(total)} !`
  //Attach an image to the message from the assets library
  const attachment = new Attachment('assets/images/'+score2asset(total));
  msg.reply(reply, attachment)
});


function rollDice(max) {
  return Math.floor(Math.random() * Math.floor(max))+1;
}
function diceToStats(n){
  if(n === 1){
    return -1;
  }
  if(n === 3){
    return 1;
  }
  return 0;
}
function signPrint(n){
  if(n < 0){
    return "-";
  }
  if(n > 0){
    return "+";
  }
  return "0";

}
function score2asset(score){
  if(score < 0){
    return "m"+parseInt(-1*score)+".png";
  }
  else {
    return "p"+parseInt(score)+".png";
  }
}
var download_file_curl = function(file_url, file_name) {
    // create an instance of writable stream
    var file = fs.createWriteStream(file_name);
    // execute curl using child_process' spawn function
    var curl = spawn('curl', [file_url]);
    // add a 'data' event listener for the spawn instance
    curl.stdout.on('data', function(data) { file.write(data); });
    // add an 'end' event listener to close the writeable stream
    curl.stdout.on('end', function(data) {
        file.end();
        console.log(file_name + ' downloaded to ' + file_name);
    });
    // when the spawn child process exits, check if there were any errors and close the writeable stream
    curl.on('exit', function(code) {
        if (code != 0) {
            console.log('Failed: ' + code);
        }
    });
};
client.login(process.env.DISCORD_TOKEN);
